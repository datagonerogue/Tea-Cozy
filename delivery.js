const formOne = document.getElementById("stepOne");
const formTwo = document.getElementById("stepTwo");
const toStepTwoButton = document.getElementById("toStepTwoButton");
const toStepOneButton = document.getElementById("toStepOneButton");
const stepOneInputs = document.querySelectorAll('#stepOne input');
const stepTwoInputs = document.querySelectorAll('#stepTwo input, #stepTwo textarea');

toStepTwoButton.addEventListener("click", () => {
  formOne.classList.toggle("hidden");
  formTwo.classList.toggle("hidden");
  setTimeout(() => {
    window.scrollTo({
        top: 0,
        behavior: 'smooth' 
    });
}, 100); 
})
toStepOneButton.addEventListener("click", () => {
     formOne.classList.toggle("hidden");
  formTwo.classList.toggle("hidden");
  setTimeout(() => {
    window.scrollTo({
        top: 0,
        behavior: 'smooth' 
    });
}, 100); 
})

const prices = {
    "club-sandwich": 5.00,
    "avocado-sandwich": 6.50,
    "banh-mi": 7.00,
    "romesco-cauliflower-sandwich": 8.25,
    "chickpea-salad": 4.75,
    "quinoa-salad": 5.50,
    "kale-salad": 6.00,
    "beetroot-and-hummus-salad": 5.75,
    "mediterranean-salad": 6.25,
    "pumpkin-spice-hot-chocolate": 3.50,
    "crockpot-apple-cider": 4.00,
    "ghostly-hot-cocoa": 2.75,
    "chai": 3.25,
    "espresso-tonic": 4.50,
    "kokkaffe": 2.00,
    "vanilla": 3.50,
    "chocolate": 4.00,
    "strawberry": 4.25,
    "durian-ice-cream": 6.00,
    "ginger-pineapple-ice-cream": 5.50,
    "lavender": 4.75,
    "flaky-sunrise-croissants": 3.50,
    "muffin-magic": 2.75,
    "cinnamon-swirl-bliss": 4.00,
    "quiches": 6.25,
    "focaccia": 3.00,
    "cheese-straws": 2.50
}
        function calculateTotal() {
            let totalCost = 0;
        stepOneInputs.forEach(input => {
const itemName = input.name;
                const itemQuantity = parseInt(input.value) || 0; 
                totalCost += itemQuantity * (prices[itemName] || 0);
        });

            document.getElementById('cost').innerText = `${totalCost.toFixed(2)}`;
        }

function startPayment() {
        const formData = new FormData();
        
        stepOneInputs.forEach(input => {
            formData.append(input.name, input.value);

        });

        stepTwoInputs.forEach(input => {

          formData.append(input.name, input.value);
        });
        console.log(formData);

  
}





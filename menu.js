const quickbitesBtn = document.getElementById("quick-bites-btn");
const quickbitesTab = document.getElementById("quick-bites-tab");
const cozydrinksBtn = document.getElementById("cozy-drinks-btn");
const cozydrinksTab = document.getElementById("cozy-drinks-tab");
const icecreamBtn = document.getElementById("ice-cream-btn");
const icecreamTab = document.getElementById("ice-cream-tab");
const bakedgoodsBtn = document.getElementById("baked-goods-btn");
const bakedgoodsTab = document.getElementById("baked-goods-tab");
function closeAllTabs() {
  quickbitesTab.classList.add("hidden");
  cozydrinksTab.classList.add("hidden");
  icecreamTab.classList.add("hidden");
  bakedgoodsTab.classList.add("hidden");
}

quickbitesBtn.addEventListener("click", () => {
  closeAllTabs();
  quickbitesTab.classList.toggle("hidden");
});

cozydrinksBtn.addEventListener("click", () => {
  closeAllTabs();
  cozydrinksTab.classList.toggle("hidden");
});

icecreamBtn.addEventListener("click", () => {
  closeAllTabs();
  icecreamTab.classList.toggle("hidden");
});

bakedgoodsBtn.addEventListener("click", () => {
  closeAllTabs();
  bakedgoodsTab.classList.toggle("hidden");
});


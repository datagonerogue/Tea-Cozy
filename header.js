const pageNavbar = document.getElementById("page-navbar");
const popupBtn = document.getElementById("popup-btn");
const popupCloseBtn = document.getElementById("popup-close-btn");
popupBtn.addEventListener("click", () => {
  pageNavbar.classList.toggle("hidden");
});
popupCloseBtn.addEventListener("click", () => {
  pageNavbar.classList.toggle("hidden");
});

document.addEventListener("keydown", (event) => {
  if (event.key === "Escape") {
    pageNavbar.classList.add("hidden");
  }
});
window.addEventListener("scroll", () => {
  if (!pageNavbar.classList.contains("hidden")) {
    pageNavbar.classList.add("hidden");
  }
});


# Cottage Comfort Website
[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

![Website](https://img.shields.io/website?url=https%3A%2F%2Fdatagonerogue.codeberg.page%2Fcottage-comfort&up_message=online&up_color=red&down_message=offline&down_color=red&style=for-the-badge&label=status&labelColor=black)
![W3C Validation](https://img.shields.io/w3c-validation/html?targetUrl=https%3A%2F%2Fdatagonerogue.codeberg.page%2Fcottage-comfort&style=for-the-badge&labelColor=black)


## Contributing

If you find any bugs or have suggestions for improvement, please open an issue or submit a pull request.

## License

This project is licensed under the [GPL 3.0 or Later](LICENSE).
